﻿using MonitoringTool.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MonitoringTool.Helpers
{
    public static class MenuItemHelper
    {
        public static HtmlString IsActive(this HtmlHelper helper, MenuItems menuItem, string classes = null)
        {
            if (helper.ViewBag.MenuItem == null || !(helper.ViewBag.MenuItem is MenuItems))
            {
                helper.ViewBag.MenuItem = MenuItems.NONE;
            }

            if (helper.ViewBag.MenuItem == menuItem)
            {
                return new HtmlString("class=\"" + (classes != null ? classes + " " : "") + "active\"");
            }

            return new HtmlString("class=\""+ (classes != null ? classes : "") +"\"");
        }
        public static void SetActive(this HtmlHelper helper, MenuItems menuItem)
        {
            helper.ViewBag.MenuItem = menuItem;
        }
    }
}