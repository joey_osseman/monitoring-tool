﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using MonitoringTool.Domain.Core;

namespace MonitoringTool.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            MongoRepository<Node> nodes = new MongoRepository<Node>();
            return View(nodes.ToList().Cast<Node>().ToList());
        }
    }
}