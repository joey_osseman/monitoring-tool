﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using MonitoringTool.Domain.Core;
using MonitoringTool.ViewModels;

namespace MonitoringTool.Controllers
{
    public class UrlNodeController : Controller
    {
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(UrlNodeCreate urlNodeCreate)
        {
            if (urlNodeCreate.Name != null && urlNodeCreate.Url != null)
            {
                MongoRepository<Node> nodes = new MongoRepository<Node>();
                nodes.Add(new UrlNode()
                {
                    Name = urlNodeCreate.Name,
                    Url = urlNodeCreate.Url
                });

                return RedirectToAction("Index", "Home");
            }

            return View(urlNodeCreate);
        }
    }
}