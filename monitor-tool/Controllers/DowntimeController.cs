﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using MonitoringTool.Domain.Core;
using MonitoringTool.Domain.Downtimes;
using MonitoringTool.ViewModels;

namespace MonitoringTool.Controllers
{
    public class DowntimeController : Controller
    {
        public ActionResult Index(string Id)
        {
            MongoRepository<Node> nodes = new MongoRepository<Node>();
            MongoRepository<Downtime> downtimes = new MongoRepository<Downtime>();
            Node node = nodes.FirstOrDefault(o => o.Id == Id);
            if (node == null)
                return RedirectToAction("Index", "Home");

            return View(downtimes.Where(o => o.NodeId == node.Id).ToList());
        }

        public ActionResult Seen(string id)
        {
            MongoRepository<Downtime> downtimes = new MongoRepository<Downtime>();
            Downtime downtime = downtimes.GetById(id);
            if (downtime != null)
            {
                downtime.Seen = true;
                downtimes.Update(downtime);
            }

            return Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetUnseenDowntime()
        {
            MongoRepository<Downtime> downtimes = new MongoRepository<Downtime>();
            MongoRepository<Node> nodes = new MongoRepository<Node>();
            return Json(downtimes.Where(o => !o.Seen).Select(x => new UnseenDowntime()
                {
                    Node = nodes.GetById(x.NodeId),
                    Downtime = x
                }).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}