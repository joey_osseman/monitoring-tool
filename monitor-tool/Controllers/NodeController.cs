﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using MonitoringTool.Domain.Core;
using MonitoringTool.ViewModels;

namespace MonitoringTool.Controllers
{
    public class NodeController : Controller
    {
        public ActionResult Remove(string Id)
        {
            MongoRepository<Node> nodes = new MongoRepository<Node>();
            nodes.Delete(Id);
            return RedirectToAction("Index", "Home");
        }
    }
}