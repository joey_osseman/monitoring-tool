﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using MonitoringTool.Domain.Core;
using MonitoringTool.ViewModels;

namespace MonitoringTool.Controllers
{
    public class ICMPNodeController : Controller
    {
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(ICMPNodeCreate icmpNodeCreate)
        {
            if (icmpNodeCreate.Host != null && icmpNodeCreate.Name != null)
            {
                MongoRepository<Node> nodes = new MongoRepository<Node>();
                nodes.Add(new ICMPNode()
                {
                    Name = icmpNodeCreate.Name,
                    Host = icmpNodeCreate.Host
                });

                return RedirectToAction("Index", "Home");
            }

            return View(icmpNodeCreate);
        }
    }
}