﻿using MongoRepository;
using MonitoringTool.Domain.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTool.Domain.Downtimes
{
    public class DowntimeRegistrator
    {
       public static void NodeEvents_NodeEvent(object sender, NodeEvent e)
        {
            MongoRepository<Downtime> Downtimes = new MongoRepository<Downtime>();
            if (e is NodeOfflineEvent)
            {
                // Start downtime
                Downtime downtime = new Downtime()
                {
                    NodeId = e.Node.Id,
                    Start = DateTime.UtcNow
                };
                Downtimes.Add(downtime);
            }
            else if (e is NodeOnlineEvent)
            {
                Downtime downtime = Downtimes.Where(o => o.Stop == null).FirstOrDefault(o => o.NodeId == e.Node.Id);
                if (downtime != null)
                {
                    downtime.Stop = DateTime.UtcNow;
                    Downtimes.Update(downtime);
                }
            }
        }
    }
}