﻿using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTool.Domain.Downtimes
{
    public class Downtime : Entity
    {
        public string NodeId { get; set; }
        public DateTime Start { get; set; }
        public DateTime ? Stop { get; set; }
        public TimeSpan Duration
        {
            get
            {
                DateTime end = DateTime.UtcNow;
                if (Stop != null)
                {
                    end = Stop.Value;
                }

                return end - Start;
            }
        }
        public bool Seen { get; set; }
    }
}