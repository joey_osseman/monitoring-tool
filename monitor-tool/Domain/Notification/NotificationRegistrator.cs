﻿using Cofano.Net.Mail;
using MongoRepository;
using MonitoringTool.Domain.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTool.Domain.Notifications
{
    public class NotificationRegistrator
    {
        public static void NodeEvents_NodeEvent(object sender, NodeEvent e)
        {
            if (e is NodeOfflineEvent)
            {
                Mailer.SendMail("support@cofano.nl", "Node " + e.Node.Name + " is offline...", "");
            }
            else if(e is NodeOnlineEvent)
            {
                Mailer.SendMail("support@cofano.nl", "Node " + e.Node.Name + " is back online!", "");
            }
        }
    }
}