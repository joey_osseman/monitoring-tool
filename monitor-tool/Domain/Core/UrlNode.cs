﻿using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTool.Domain.Core
{
    public class UrlNode : Node
    {
        public string Url { get; set; }
    }
}