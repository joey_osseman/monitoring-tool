﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTool.Domain.Core
{
    public class ICMPNode : Node
    {
        public string Host { get; set; }
    }
}