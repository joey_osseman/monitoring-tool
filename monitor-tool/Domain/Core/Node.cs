﻿using MongoRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTool.Domain.Core
{
    public abstract class Node : Entity
    {
        public string Name { get; set; }
        public bool Online { get; set; }
    }
}