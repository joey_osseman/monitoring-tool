﻿using MonitoringTool.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTool.Domain.Events
{
    public class NodeOfflineEvent : NodeEvent
    {
        public NodeOfflineEvent(Node node)
        {
            this.Node = node;
        }
    }
}