﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTool.Domain.Events
{
    public class NodeEvents
    {
        public delegate void NodeEventHandler(object sender, NodeEvent e);
        public static event NodeEventHandler NodeEvent;
        public static void OnNodeEvent(object sender, NodeEvent e)
        {
            if (NodeEvent != null)
            {
                NodeEvent(sender, e);
            }
        }
    }
}