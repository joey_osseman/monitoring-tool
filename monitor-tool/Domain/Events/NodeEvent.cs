﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MonitoringTool.Domain.Core;
namespace MonitoringTool.Domain.Events
{
    public abstract class NodeEvent : EventArgs
    {
        public Node Node { get; protected set; }
    }
}