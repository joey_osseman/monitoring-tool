﻿using Cofano.Net;
using MongoRepository;
using MonitoringTool.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace MonitoringTool.Domain.Monitor
{
    public class UrlNodeChecker
    {
        public static void CheckNode(UrlNode node)
        {
            try
            {
                Network net = new Network(node.Url, NetworkRequestType.GET);
                net.WebRequest.Timeout = 10000;
                node.Online = net.GetResponseString().Equals("OK");
            }
            catch (Exception)
            {
                node.Online = false;
            }
        }
    }
}