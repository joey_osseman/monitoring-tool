﻿using MongoRepository;
using MonitoringTool.Domain.Core;
using MonitoringTool.Domain.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace MonitoringTool.Domain.Monitor
{
    public class Monitor
    {
        private static Thread monitoringThread;
        private static bool running = true;

        public static void Start()
        {
            if (monitoringThread == null)
            {
                monitoringThread = new Thread(new ThreadStart(Run))
                {
                    Priority = ThreadPriority.Lowest,
                    IsBackground = true
                };
                monitoringThread.Start();

            }
        }

        private static void Run() 
        {
            const int interval = 1000; // Every minute.
            while (running)
            {
                MongoRepository<Node> nodes = new MongoRepository<Node>();
                Parallel.ForEach(nodes.Where(o => o is UrlNode || o is ICMPNode), o =>
                {
                    bool original = o.Online;
                    if (o is UrlNode) 
                        UrlNodeChecker.CheckNode((UrlNode)o);
                    if (o is ICMPNode)
                        ICMPNodeChecker.CheckNode((ICMPNode)o);

                    nodes.Update(o);
                    if (original != o.Online)
                    {
                        NodeEvents.OnNodeEvent(Thread.CurrentThread, (o.Online ? (NodeEvent)new NodeOnlineEvent(o) : new NodeOfflineEvent(o)));
                    }
                });

                Thread.Sleep(interval);
            }

            monitoringThread = null;
        }


        public static void Stop()
        {
            running = false;
        }
    }
}