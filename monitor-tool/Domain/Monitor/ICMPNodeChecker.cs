﻿using MongoRepository;
using MonitoringTool.Domain.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;

namespace MonitoringTool.Domain.Monitor
{
    public class ICMPNodeChecker
    {
        public static void CheckNode(ICMPNode node)
        {
            try
            {
                bool reachable = false;
                for (int i = 0; i < 4; i++)
                {
                    if (new Ping().Send(node.Host, 2000).Status == IPStatus.Success)
                    {
                        reachable = true;
                        break;
                    }
                }

                node.Online = reachable;
            }
            catch (Exception)
            {
                node.Online = false;
            }
        }
    }
}