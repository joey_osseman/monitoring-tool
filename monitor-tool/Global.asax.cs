﻿using MonitoringTool.Domain.Downtimes;
using MonitoringTool.Domain.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MonitoringTool
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            MonitoringTool.Domain.Monitor.Monitor.Start();
            MonitoringTool.Domain.Events.NodeEvents.NodeEvent += DowntimeRegistrator.NodeEvents_NodeEvent;
            MonitoringTool.Domain.Events.NodeEvents.NodeEvent += NotificationRegistrator.NodeEvents_NodeEvent;
        }

        protected void Application_End()
        {
            MonitoringTool.Domain.Monitor.Monitor.Stop();
            MonitoringTool.Domain.Events.NodeEvents.NodeEvent -= DowntimeRegistrator.NodeEvents_NodeEvent;
            MonitoringTool.Domain.Events.NodeEvents.NodeEvent -= NotificationRegistrator.NodeEvents_NodeEvent;
        }
    }
}
