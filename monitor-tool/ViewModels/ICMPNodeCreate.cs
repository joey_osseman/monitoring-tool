﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MonitoringTool.ViewModels
{
    public class ICMPNodeCreate
    {
        public string Name { get; set; }
        public string Host { get; set; }
    }
}