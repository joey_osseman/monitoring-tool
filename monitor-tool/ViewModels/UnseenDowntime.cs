﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonitoringTool.ViewModels
{
    public class UnseenDowntime
    {
        public Domain.Core.Node Node { get; set; }

        public Domain.Downtimes.Downtime Downtime { get; set; }
    }
}
