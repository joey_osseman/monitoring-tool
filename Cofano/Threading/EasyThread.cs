﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Cofano.Threading
{
    public abstract class EasyThread : IDisposable
    {
        protected Thread _Thread;

        public EasyThread()
        {
            _Thread = new Thread(new ThreadStart(Run));
        }

        public abstract void Run();

        public void Start()
        {
            if (_Thread != null)
                _Thread.Start();
        }
        
        [Obsolete]
        public void Suspend()
        {
            if (_Thread != null)
                _Thread.Suspend();
        }

        [Obsolete]
        public void Resume() 
        {
            if(_Thread != null)
                _Thread.Resume();
        }

        public virtual void Dispose()
        {
            if (_Thread != null)
            {
                try
                {

                    _Thread.Join(0);
                }
                catch (Exception)
                {
                    //supress
                }
                _Thread = null;
            }
        }
    }
}
