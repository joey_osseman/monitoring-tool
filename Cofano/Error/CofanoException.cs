﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cofano.Net.Mail;
using System.Configuration;
using System.IO;

namespace Cofano.Error
{
    
    public class CofanoException : ApplicationException
    {
        public CofanoException()
            : base()
        {
        }
        public CofanoException(string message)
            : base(message)
        {
        }
        public CofanoException(string message, Exception e)
            : base(message, e)
        {
        }

        public virtual string GetLog(bool first)
        {
            StringBuilder body = new StringBuilder();
            if (first)
            {
                body.Append("<h1>" + ExceptionExtension.ApplicationName + " error.</h1>");
                body.Append("<b>Utc time:</b> " + DateTime.UtcNow.ToLongTimeString());
                body.Append("<br />\r\n");
                body.Append("<b>Utc date:</b> " + DateTime.UtcNow.ToLongDateString());
                body.Append("<br />\r\n");
                body.Append("<br />\r\n");
                body.Append("<br />\r\n");
                body.Append("<b>Gmt+1 time:</b> " + DateTime.UtcNow.AddHours(1).ToLongTimeString());
                body.Append("<br />\r\n");
                body.Append("<b>Gmt+1 date:</b> " + DateTime.UtcNow.AddHours(1).ToLongDateString());
                body.Append("<br />\r\n");
            }

            body.AppendFormat("<{0}>" + this.GetType().Name + "</{0}>", first ? "h2" : "h3");
            body.Append("<b>Message:</b> " + this.Message);
            body.Append("<br />\r\n");
            body.Append("<b>Source:</b> " + this.Source);
            body.Append("<br />\r\n");
            body.Append("<b>Stack trace:</b>" + this.StackTrace);
            body.Append("<br />\r\n");

            body.Append("<br />\r\n");
            body.Append("<br />\r\n");

            if (this.InnerException != null)
            {
                body.Append(this.InnerException.GetLog(false));
            }

            if (first)
            {
                body.Append("<br />\r\n");
                body.Append("<br />\r\n");
            }
            return body.ToString();
        }
    }
}
