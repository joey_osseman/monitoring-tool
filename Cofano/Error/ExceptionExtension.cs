﻿using Cofano.Net.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace Cofano.Error
{
    /// <summary>
    /// All exception thrown by cofano services or applications should derive from this class for logging.
    /// AppSettings required:
    ///     MailExceptionRecipient. (Seperate by | for multiple recipients)
    ///     MailExceptionFrom.
    ///     ApplicationName.
    ///     LogExceptionPath
    ///     Mailer appsettings.
    /// </summary>
    /// 
    public static class ExceptionExtension
    {
        private static string _MailExceptionRecipient = "";
        private static string _MailExceptionSender = "";
        private static string _ApplicationName = "";
        private static string _LogExceptionPath = "";
        private static string _LogExceptionFileName = "";

        /// <summary>
        /// Only used when "MailExceptionRecipient" is not set in the configuration.
        /// </summary>
        public static string MailExceptionRecipient
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("MailExceptionRecipient") ?? _MailExceptionRecipient;
            }
            set { _MailExceptionRecipient = value; }
        }

        /// <summary>
        /// Only used when "MailExceptionSender" is not set in the configuration.
        /// </summary>
        public static string MailExceptionSender
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("MailExceptionFrom") ?? _MailExceptionSender;
            }
            set { _MailExceptionSender = value; }
        }

        /// <summary>
        /// Only used when "ApplicationName" is not set in the configuration.
        /// </summary>
        public static string ApplicationName
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("ApplicationName") ?? _ApplicationName;
            }
            set { _ApplicationName = value; }
        }

        /// <summary>
        /// Only used when "LogExceptionPath" is not set in the configuration.
        /// </summary>
        public static string LogExceptionPath
        {
            get
            {
                return ConfigurationManager.AppSettings.Get("LogExceptionPath") ?? _LogExceptionPath;
            }
            set { _LogExceptionPath = value; }
        }

        /// <summary>
        /// Is "ApplicationName" by default.
        /// Extension not included.
        /// </summary>
        public static string LogExceptionFileName
        {
            get
            {
                if (String.IsNullOrWhiteSpace(_LogExceptionFileName))
                    return ApplicationName;
                return _LogExceptionFileName;
            }
            set { _LogExceptionFileName = value; }
        }

        public static string GetLog(this Exception e, bool first = true)
        {
            if(e is CofanoException)
                return ((CofanoException)e).GetLog(first);

            StringBuilder body = new StringBuilder();
            if (first)
            {
                body.Append("<h1>" + ApplicationName + " error.</h1>");
                body.Append("<b>Utc time:</b> " + DateTime.UtcNow.ToLongTimeString());
                body.Append("<br />\r\n");
                body.Append("<b>Utc date:</b> " + DateTime.UtcNow.ToLongDateString());
                body.Append("<br />\r\n");
                body.Append("<br />\r\n");
                body.Append("<br />\r\n");
                body.Append("<b>Gmt+1 time:</b> " + DateTime.UtcNow.AddHours(1).ToLongTimeString());
                body.Append("<br />\r\n");
                body.Append("<b>Gmt+1 date:</b> " + DateTime.UtcNow.AddHours(1).ToLongDateString());
                body.Append("<br />\r\n");
            }

            body.AppendFormat("<{0}>" + e.GetType().Name + "</{0}>", first ? "h2" : "h3");
            body.Append("<b>Message:</b> " + e.Message);
            body.Append("<br />\r\n");
            body.Append("<b>Source:</b> " + e.Source);
            body.Append("<br />\r\n");
            body.Append("<b>Stack trace:</b>" + e.StackTrace);
            body.Append("<br />\r\n");
            body.Append("<br />\r\n");

            if (e.InnerException != null)
            {
                body.Append(e.InnerException.GetLog(false));
            }

            if (first)
            {
                body.Append("<br />\r\n");
                body.Append("<br />\r\n");
            }
            return body.ToString();
        }
        public static void MailLog(this Exception e)
        {
            try
            {
                Mailer.SendMail(MailExceptionRecipient.Split('|'), "Exception opgetreden in " + ApplicationName, e.GetLog());
            }
            catch (Exception)
            {
                Console.Out.Write(e.GetLog());
            }
            
        }
        public static void WriteLog(this Exception e)
        {
            try
            {
                
                Directory.CreateDirectory(LogExceptionPath);
                File.AppendAllText(LogExceptionPath + Path.DirectorySeparatorChar + LogExceptionFileName + ".html", "\r\n\r\n------------------------------------------------------\r\n\r\n" + e.GetLog(), Encoding.UTF8);
                Console.Out.WriteLine("Nieuwe error in +" + LogExceptionPath??"");
            }
            catch (Exception)
            {
                Console.Out.Write(e.GetLog());
            }
        }
    }
}
