﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cofano.Reflection;
using System.Reflection;

namespace Cofano.Xml
{

    /// <summary>
    /// Abstract class to create Xml files with
    /// </summary>
    public abstract class XmlWriter
    {
        /// <summary>
        /// The size of the xml.
        /// </summary>
        long size = 0;

        /// <summary>
        /// The elements we jumped into in the xml document.
        /// </summary>
        List<string> _CurrentElement = new List<string>();

        /// <summary>
        /// Wether the current element is open or not.
        /// </summary>
        bool _AddingAttribute = false;

        /// <summary>
        /// Writes the xml header to the document
        /// </summary>
        public void StartDocument()
        {
            Write("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        }

        /// <summary>
        /// Writes a start tag for the specified element.
        /// </summary>
        /// <param name="element">The name of the element</param>
        public void StartElement(string element)
        {
            if (_CurrentElement.Count != 0)
            {
                if (_AddingAttribute)
                {
                    Write(">");
                }
            }

            Write("<"+element);
            _CurrentElement.Add(element);
            _AddingAttribute = true;
        }

        /// <summary>
        /// Writes a start tag for the specified element with the given attributes
        /// </summary>
        /// <param name="element"></param>
        /// <param name="attributes"></param>
        public void StartElement(string element, dynamic attributes)
        {
            StartElement(element);
            WriteAttributes(attributes);
        }

        /// <summary>
        /// Adds the attribute to the current element. Not available when WriteValue has been called.
        /// </summary>
        /// <param name="attribute">The name of the attribute.</param>
        /// <param name="value">The value of the attribute</param>
        public void WriteAttribute(string attribute, string value)
        {
            if(_AddingAttribute)
                Write(" " + attribute + "=\"" + value + "\"");
        }

        /// <summary>
        /// Adds the given attributes to the current Element. Not available when WriteValue has been called.
        /// </summary>
        /// <param name="attributes">An anonymous type with an Attribute and Value member.</param>
        public void WriteAttributes(dynamic attributes)
        {
            foreach (PropertyInfo attribute in attributes.GetType().GetProperties())
            {
                WriteAttribute(attribute.Name, attribute.GetValue(attributes,null).ToString());
            }
        }

        /// <summary>
        /// Adds the value to the current element. Closes the start tag when its open.
        /// </summary>
        /// <param name="value">The value to add.</param>
        public void WriteValue(string value)
        {
            if (_CurrentElement.Count != 0 && !String.IsNullOrEmpty(value))
            {
                if (_AddingAttribute)
                {
                    Write(">");
                    _AddingAttribute = false;
                }
                Write(value);
            }
        }

        /// <summary>
        /// Creates an element with the given value.
        /// </summary>
        /// <param name="element">The element name</param>
        /// <param name="value">The value of the element</param>
        public void AddValueElement(string element, string value)
        {
            StartElement(element);
            WriteValue(value);
            EndElement();
        }

        /// <summary>
        /// Creates the given element with the attributes, and closes it inmiddelation
        /// </summary>
        /// <param name="element"></param>
        /// <param name="attributes"></param>
        public void AddAttributeElement(string element, dynamic attributes)
        {
            StartElement(element);
            WriteAttributes(attributes);
            EndElement();
        }

        /// <summary>
        /// Closes the current element
        /// </summary>
        public void EndElement()
        {
            if (_AddingAttribute)
            {
                Write(" />");
                _AddingAttribute = false;
            }
            else
                Write("</" + _CurrentElement.Last() + ">");

            _CurrentElement.RemoveAt(_CurrentElement.Count - 1);
        }

        /// <summary>
        /// Returns the length of the current Xml Document without closing tags.
        /// </summary>
        /// <returns>The length of the current Xml Document without closing tags.</returns>
        public long Size()
        {
            return size;
        }

        /// <summary>
        /// Returns the length of the xml document with closing tags included.
        /// </summary>
        /// <returns>The length of the xml document with closing tags included.</returns>
        public long SizeIncludingCloseTags()
        {
            long sizeincluding = size;
            if (_CurrentElement.Count != 0)
            {
                int i = 0;
                foreach (string currentElement in _CurrentElement)
                {
                    if (i == 0 && _AddingAttribute)
                        sizeincluding += 3;
                    else
                        sizeincluding += 3 + currentElement.Length;
                }
            }
            return sizeincluding;
        }

        /// <summary>
        /// Wrapper to add xml. Updates the string length, then passes it on to the derived class.
        /// </summary>
        /// <param name="xml">The xml to add</param>
        private void Write(string xml)
        {
            size += xml.Length;
            WriteXml(xml);
        }

        /// <summary>
        /// Writes the xml to the medium
        /// </summary>
        /// <param name="xml">the xml to write</param>
        protected abstract void WriteXml(string xml);
    }
}
