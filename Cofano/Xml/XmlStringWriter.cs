﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cofano.Xml
{
    /// <summary>
    /// Class to create xml files in to strings
    /// </summary>
    public class XmlStringWriter : XmlWriter
    {
        /// <summary>
        /// The stringbuilder containing the xml being build.
        /// </summary>
        StringBuilder _Xml = new StringBuilder();

        /// <summary>
        /// Creates a string of the generated xml.
        /// </summary>
        /// <returns>The generated xml.</returns>
        public override string ToString()
        {
            return _Xml.ToString();
        }

        /// <summary>
        /// Returns the generated xml in bytes, using the UTF-8 encoder.
        /// </summary>
        /// <returns>Xml in bytes</returns>
        public byte[] ToBytes()
        {
            return ToBytes(Encoding.UTF8);
        }

        /// <summary>
        /// Returns the generated xml in bytes.
        /// </summary>
        /// <param name="encoder">The encoding method to encode the xml</param>
        /// <returns>The xml in bytes</returns>
        public byte[] ToBytes(Encoding encoder)
        {
            return encoder.GetBytes(_Xml.ToString());
        }

        /// <summary>
        /// Writes the xml to the medium
        /// </summary>
        /// <param name="xml">the xml to write</param>
        protected override void WriteXml(string xml)
        {
            _Xml.Append(xml);
        }
    }
}
