﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace Cofano.Xml
{
    /// <summary>
    /// XmlReader that automatically closes the stream assiociated with it.
    /// </summary>
    public class XmlResponseReader : IDisposable
    {
        /// <summary>
        /// The input stream connected to the xmlreader.
        /// </summary>
        Stream _InputStream = null;

        /// <summary>
        /// The underlaying xmlreader thats supplies us with xml.
        /// </summary>
        public XmlReader _XmlReader = null;

        /// <summary>
        /// List of parent nodes. The last node is the current parent node.
        /// </summary>
        private List<string> CurrentNode = new List<string>();

        /// <summary>
        /// Whether to skip when moving to next element.
        /// False when stepped into element, true when moved to next element.
        /// </summary>
        private bool DoSkip = false;

        /// <summary>
        /// Makes an XmlResponseReader by stream.
        /// </summary>
        /// <param name="input">The stream to read the xml from.</param>
        public XmlResponseReader(Stream input)
        {
            if (input == null)
                throw new ArgumentNullException("Stream is null");
            _InputStream = input;
            _XmlReader = XmlReader.Create(input);
        }

        /// <summary>
        /// Makes an XmlResponseReader from another reader.
        /// </summary>
        /// <param name="reader">The reader to read the xml from.</param>
        protected XmlResponseReader(XmlReader reader)
        {
            _XmlReader = reader;
        }

        /// <summary>
        /// Moves to the next element with the given name.
        /// Does not look in to child objects.
        /// </summary>
        /// <param name="elementName">The name of the element to go to.</param>
        /// <returns>The response reader to enable chaining</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">When the element was not found in the current element.</exception>
        public XmlResponseReader MoveToElement(string elementName)
        {
            if(DoSkip)
                _XmlReader.Skip();
            DoSkip = true;
            while (_XmlReader.Read())
            {
                if (_XmlReader.NodeType == XmlNodeType.Element)
                {
                    if (_XmlReader.Name == elementName)
                        return this;
                    _XmlReader.Skip();
                }
                if (_XmlReader.NodeType == XmlNodeType.EndElement && CurrentNode.Count > 0) 
                {
                    if (_XmlReader.Name.Equals(CurrentNode.Last()))
                        throw new ArgumentOutOfRangeException("Element not found.");
                }
            }
            throw new ArgumentOutOfRangeException("Element not found.");
        }
        
        /// <summary>
        /// Moves the pointer to the next element.
        /// </summary>
        /// <returns>If an element was reached.</returns>
        public bool MoveToNextElement()
        {
            if (DoSkip)
                _XmlReader.Skip();
            DoSkip = true;
            while (_XmlReader.Read())
            {
                if (_XmlReader.NodeType == XmlNodeType.Element)
                {
                    return true;
                }
                if (_XmlReader.NodeType == XmlNodeType.EndElement && CurrentNode.Count > 0)
                {
                    if (_XmlReader.Name.Equals(CurrentNode.Last()))
                        return false;
                }
            }
            return false;
        }


        /// <summary>
        /// Steps into the current element.
        /// </summary>
        /// <returns>True if successfully stepped into, else false.</returns>
        public bool StepIntoCurrentElement()
        {
            if (_XmlReader.NodeType == XmlNodeType.Element && !_XmlReader.IsEmptyElement)
            {
                DoSkip = false;
                CurrentNode.Add(_XmlReader.Name);
                _XmlReader.Read();
                return true;
            }
            return false;
        }
        
        /// <summary>
        /// Steps out of the current element by moving to its end tag.
        /// Cannot be called when in the root element.
        /// </summary>
        public void StepOutOfCurrentElement()
        {
            if (CurrentNode.Count <= 0)
                throw new InvalidOperationException("Cannot step out of element when in the root element.");
            do
            {
                if (_XmlReader.NodeType == XmlNodeType.Element)
                {
                    if (_XmlReader.Name == CurrentNode.Last())
                        _XmlReader.Read();
                    else
                        _XmlReader.Skip();
                }
                if (_XmlReader.NodeType == XmlNodeType.EndElement)
                {
                    if (_XmlReader.Name == CurrentNode.Last())
                    {
                        CurrentNode.Remove(_XmlReader.Name);
                        DoSkip = true;
                        break;
                    }
                }
            } while (_XmlReader.Read());
        }

        /// <summary>
        /// Moves to the given element, and steps into it.
        /// </summary>
        /// <param name="elementName">The element to find and step into.</param>
        /// <returns>The current XmlResponseReader to enable chaining.</returns>
        public XmlResponseReader StepIntoElement(string elementName)
        {
            MoveToElement(elementName);
            StepIntoCurrentElement();
            return this;
        }

        /// <summary>
        /// Gets the given element as a new stream object.
        /// When the return object is closed, this object jumps to the end of the node.
        /// 
        /// This makes it easier to loop trough an array of elements.
        /// <example>
        /// while((reader = GetElementReader("elementName")) != null) 
        /// {
        ///     // Do stuff
        ///     reader.Close();
        /// }
        /// </example>
        /// </summary>
        /// <param name="elementName">The element to open in the new responsereader.</param>
        /// <returns>A new ResponseReader to read xml from or null when the element was not found.</returns>
        /// <exception cref="System.InvalidOperationException">When the current node is empty or not an element.</exception>
        public XmlResponseReader GetElementReader(string elementName)
        {
            try
            {
                // Move to the desired element.
                MoveToElement(elementName);
            }
            catch (Exception)
            {
                // element not found, return null.
                return null;
            }

            // check if not a element selected or element is empty.
            if(_XmlReader.NodeType != XmlNodeType.Element || _XmlReader.IsEmptyElement)
                throw new InvalidOperationException("The current node selected is not an element or is empty.");

            // return the response reader.
            return new XmlResponseReader(_XmlReader.ReadSubtree());
        }

        /// <summary>
        /// Peform actions on a XmlResponseReader on all elements found with the given name.
        /// The XmlReponseReaders are returned by GetElementReader.
        /// ReponseReaders are automatically closed and disposed.
        /// </summary>
        /// <param name="elementNames">The name of the elements.</param>
        /// <param name="action">The delegate that performs actions on the object.</param>
        /// <exception cref="System.InvalidOperationException">When action is null.</exception>
        /// <exception cref="System.InvalidOperationException">When the the reader could not be made.</exception>
        public void ForeachElement(string elementNames, Action<XmlResponseReader> action)
        {
            if (action == null)
                throw new ArgumentNullException("Action cannot be null.");
            XmlResponseReader reader = null;
            while ((reader = GetElementReader(elementNames)) != null) 
            {
                reader.MoveToElement(elementNames);
                action(reader);
                reader.Close();
            }
        }



        /// <summary>
        /// Disposes the stream
        /// </summary>
        public void Dispose()
        {
            if (_InputStream != null)
            {
                try
                {
                    _InputStream.Dispose();
                }
                catch (Exception)
                {
                    //supress
                }
                finally
                {
                    _InputStream = null;
                }
            }
            if (_XmlReader != null)
            {
                try
                {
                    _XmlReader.Close();
                }
                catch (Exception)
                {
                    // Supress the warnings, probably already closed.
                    _XmlReader = null;
                }
            }
        }

        /// <summary>
        /// Gets the name of the current element.
        /// </summary>
        public string ElementName
        {
            get
            {
                return _XmlReader.Name;
            }
        }

        /// <summary>
        /// Gets the parent element of the current element or "" when root.
        /// </summary>
        public string ParentElement
        {
            get
            {
                return IsRootElement ? "" : CurrentNode.Last();
            }
        }

        /// <summary>
        /// Gets if the current parent element is the root.
        /// </summary>
        public bool IsRootElement
        {
            get
            {
                return CurrentNode.Count == 0;
            }
        }

        /// <summary>
        /// Closes the current xmlstream.
        /// When called on an xmlstream that is a child of another stream, the parent stream moves to the next sibling.
        /// </summary>
        public void Close()
        {
            try
            {
                _XmlReader.Close();
            }
            catch (Exception)
            {
                //Suppress errors, we dont want to do anything with it.
            }
            finally
            {
                _XmlReader = null;
                this.Dispose();
            }
        }

        /// <summary>
        /// Returns if the current element has the given attribute.
        /// </summary>
        /// <param name="attrName">The attribute name</param>
        /// <returns>Yes if the attribute exists on the element, false if the attribute was not found.</returns>
        /// <exception cref="System.InvalidOperationException">When the pointer is not on an element.</exception>
        public bool HasAttribute(string attrName)
        {
            if (_XmlReader.NodeType != XmlNodeType.Element)
                throw new InvalidOperationException("Pointer is not on an element");

            try
            {
                GetAttribute(attrName);
                return true;
            }
            catch (Exception)
            { 
                // we dont care about the exception, the attribute is just not available.
                // Supress the exception.
            }
            return false;
        }
        
        /// <summary>
        /// Gets the value of the specified attribute.
        /// </summary>
        /// <param name="attrName">The attribute name to return the value from.</param>
        /// <returns>The value of the attribute</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">When the attribute was not found or was string.empty.</exception>
        public string GetAttribute(string attrName)
        {
            if (_XmlReader.NodeType != XmlNodeType.Element)
                throw new InvalidOperationException("Pointer is not on an element.");
            string value = _XmlReader.GetAttribute(attrName);
            if(value != null)
                return value;
            throw new ArgumentOutOfRangeException("Attribute not found.");
        }

        /// <summary>
        /// Gets the value of the specified attribute and tries to cast it to int.
        /// </summary>
        /// <param name="attrName">The name of the attribute</param>
        /// <returns>The value of the attribute or -1 when failed.</returns>
        public int GetIntAttribute(string attrName)
        {
            try
            {
                int value = int.Parse(GetAttribute(attrName));
                return value;
            }
            catch (Exception) { }
            return -1;
        }

        /// <summary>
        /// Gets the value of the specified attribute and tries to cast it to boolean.
        /// </summary>
        /// <param name="attrName">The name of the attribute</param>
        /// <returns>True when value equals true or 1. False when not true or 1 or is empty.</returns>
        public bool GetBoolAttribute(string attrName)
        {
            try
            {
                string value = GetAttribute(attrName);
                return value.ToLowerInvariant().Equals("true") || value.Equals("1");
            }
            catch (Exception) { }
            return false;
        }

        /// <summary>
        /// Gets the contents of the current element as string,
        /// and moves to the closing tag of the element.
        /// </summary>
        /// <returns>The element content</returns>
        /// <exception cref="System.InvalidOperationException">When the current node is not an element or when the element selected has no content.</exception>
        public string GetCurrentElementContents()
        {
            if(_XmlReader.NodeType != XmlNodeType.Element)
                throw new InvalidOperationException("No element selected");
            if (_XmlReader.IsEmptyElement)
                throw new InvalidOperationException("Element was empty.");

            StepIntoCurrentElement();
            try
            {
                return _XmlReader.ReadString();
            }
            catch (Exception)
            {
                // Let the caller handle the exception, just try catch for the finally tag.
                throw;
            }
            finally
            {
                StepOutOfCurrentElement();
            }
        }
    }
}
