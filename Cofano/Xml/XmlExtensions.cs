﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Cofano.Xml
{
    public static class XmlExtensions
    {
        public static bool GoToNode(this XmlReader reader, string nodeName)
        {
            while (reader.Read())
            {
                if (reader.IsStartElement())
                {
                    if (reader.Name.Equals(nodeName))
                    {
                        return true;
                    }
                }
                reader.Skip();
            }
            return false;
        }
        



       
        public static XmlReader ReadSubtree(this XmlReader reader, string nodeName)
        {
            if (reader.GoToNode(nodeName))
            {
                XmlReader subtree = reader.ReadSubtree();
                subtree.ReadStartElement();
                return subtree;
            }
            return null;
        }
        public static string GetNodeValue(this XmlReader reader, string nodeName)
        {
            reader.GoToNode(nodeName);
            reader.ReadStartElement(nodeName);
            return reader.ReadString();
        }
    }
}
