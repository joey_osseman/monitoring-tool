﻿using Cofano.Threading;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;

namespace Cofano.Time
{
    public abstract class DailyJob : EasyThread
    {
        int hour = 0;
        int minutes = 0;
        bool running = false;

        /// <summary>
        /// Creates a new DailyJob object
        /// </summary>
        /// <param name="time">Time in the format "HH:MM"</param>
        protected DailyJob(string time)
        {
            string[] preferedTime = time.Split(':');
            try
            {
                hour = Int32.Parse(preferedTime[0]);
                minutes = Int32.Parse(preferedTime[1]);
            }
            catch (Exception)
            {
                hour = 2;
                minutes = 0;
            }
        }
        /// <summary>
        /// Creates a new DailyJob object
        /// </summary>
        /// <param name="hours">The hour part of the time to execute. (On a 24h scale)</param>
        /// <param name="minutes">The minutes part of the time.</param>
        protected DailyJob(int hours, int minutes)
        {
            this.hour = hours;
            this.minutes = minutes;
        }

        public override void Run()
        {
            DateTime executionTime = DateTime.Now; // just as temporary reference;
            running = true;
            executionTime = new DateTime(executionTime.Year, executionTime.Month, executionTime.Day, hour, minutes, executionTime.Second);

            while (running)
            {
                DoJob();
                if (executionTime < DateTime.Now)
                {
                    executionTime= executionTime.AddDays(1);
                }
                Thread.Sleep(executionTime - DateTime.Now);
            }
        }

        public abstract void DoJob();

        public override void Dispose()
        {
            if (_Thread != null)
            {
                running = false;
                try
                {
                    _Thread.Interrupt();
                }
                catch (Exception) { }
            }
            base.Dispose();
        }

    }
}
