﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Cofano.IO
{
    public static class FileHelper
    {
        /// <summary>
        /// Append all with suppressing errors.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool AppendText(string path, string text)
        {
            try
            {
                File.AppendAllText(path, text, Encoding.UTF8);
                return true;
            }
            catch (Exception)
            {
                //supress exception
            }
            return false;
        }
    }
}
