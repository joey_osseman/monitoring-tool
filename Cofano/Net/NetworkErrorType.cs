﻿namespace Cofano.Net
{
    public enum NetworkErrorType
    {
        INVALID_URL,
        NO_CONNECTION,
        PROGRAM_ERROR,
        RESPONSE_ERROR
    }
}
