﻿using System.Net;
using System.Collections.Generic;
using System;
using System.Security;
using System.IO;
using System.Text;
using System.Web;
using System.Linq;
using Cofano.Xml;

namespace Cofano.Net
{
    public class Network
    {
        private HttpWebRequest _WebRequest = null;
        private HttpWebResponse _WebResponse = null;
        private List<KeyValuePair<string,string>> _PostVars = new List<KeyValuePair<string,string>>();
        private List<string> _Files = new List<string>();
        private byte[] _PostBytes = null;
        private NetworkRequestType _RequestType = NetworkRequestType.POST;
        private string _Boundary = "Hoi wij zijn Joey en Jelle! :>";
        private byte[] _Response = null;
        private bool _MadeResponse = false;

        /// <summary>
        /// Gets the postvars supplied.
        /// </summary>
        public List<KeyValuePair<string, string>> PostVars
        {
            get { return _PostVars.ToList(); }
        }

        /// <summary>
        /// The HttpWebRequest used for the request.
        /// </summary>
        public HttpWebRequest WebRequest
        {
            get { return _WebRequest; }
            private set 
            {
                if (value == null)
                    throw new ArgumentNullException("Webrequest cannot be null.");
                _WebRequest = value; 
            }
        }

        /// <summary>
        /// The HttpWebRequest used for the response
        /// </summary>
        protected HttpWebResponse WebResponse
        {
            get { return _WebResponse; }
            private set { _WebResponse = value; }
        }

        /// <summary>
        /// Gets or sets the boundary used for the multipart.
        /// </summary>
        public string Boundary
        {
            get { return _Boundary; }
            set { _Boundary = value; }
        }


        /// <summary>
        /// Gets the type of request.
        /// </summary>
        public NetworkRequestType RequestType
        {
            get { return _RequestType; }
            protected set { _RequestType = value; }
        }

        /// <summary>
        /// Returns the content type of the response.
        /// </summary>
        public string ResponseContentType
        {
            get
            {
                if (WebResponse == null)
                    throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:026 - No response has been made. Cannot return response content type.");
                return WebResponse.ContentType;
            }
        }


        /// <summary>
        /// Initiates a Network object.
        /// </summary>
        /// <param name="URI">The url to connect to.</param>
        /// <param name="RequestType">The type request</param>
        public Network(string URI, NetworkRequestType requestType) 
        {
            RequestType = requestType;
            try
            {
                HttpWebRequest webRequest = HttpWebRequest.Create(URI) as HttpWebRequest;
                if (webRequest == null)
                    throw new UriFormatException("Cofano.Net.Network:001 - Must be HTTP or HTTPS uri scheme.");
                WebRequest = webRequest;
            }
            catch (NotSupportedException e)
            {
                //The request scheme specified in requestUriString has not been registered.
                throw new NetworkException(NetworkErrorType.INVALID_URL, "Cofano.Net.Network:002 - Invalid scheme -> HTTP, HTTPS.", e);
            }
            catch (ArgumentNullException e)
            {
                //requestUriString is null.
                throw new NetworkException(NetworkErrorType.INVALID_URL, "Cofano.Net.Network:003 - No URI provided.", e);
            }
            catch (SecurityException e)
            {
                //The caller does not have permission to connect to the requested URI or a URI that the request is redirected to.
                throw new NetworkException(NetworkErrorType.INVALID_URL, "Cofano.Net.Network:004 - URI is not accesible. Not enough rights to connect to the scheme.", e);
            }
            catch (UriFormatException e)
            {
                //The URI specified in requestUriString is not a valid URI.
                throw new NetworkException(NetworkErrorType.INVALID_URL, "Cofano.Net.Network:005 - Invalid URI format.", e);
            }
            catch (FormatException e)
            {
                //The URI specified in requestUriString is not a valid URI. (For Windows Store apps && Portable Class libraries)
                throw new NetworkException(NetworkErrorType.INVALID_URL, "Cofano.Net.Network:006 - Invalid URI format.", e);
            }

            WebRequest.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
            WebRequest.Method = ((int)RequestType < 15) ? "POST" : "GET";
            WebRequest.AllowWriteStreamBuffering = true;
        }

        /// <summary>
        /// Initiates a Network object.
        /// </summary>
        /// <param name="URL">The url to connect to.</param>
        public Network(string URI)
            : this(URI, NetworkRequestType.POST)
        {
        }
        
        public Stream GetResponseStream()
        {
            if (this._MadeResponse)
                throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:025 - Cannot open the response stream twice.");
            MakeRequest();
            try
            {
                WebResponse = WebRequest.GetResponse() as HttpWebResponse;
                if (WebResponse == null)
                    throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:024 - Response has to be of type HttpWebResponse.");
                this._MadeResponse = true;
                return WebResponse.GetResponseStream();
            }
            catch (Exception e)
            {
                if (e is NetworkException) throw;
                throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:021 - Error getting the response.", e);
            }
        }

        /// <summary>
        /// Gets a XmlResponseStream. This stream automatically closes the reponse stream.
        /// </summary>
        /// <returns>The XmlResponseReader</returns>
        public XmlResponseReader GetXmlResponseStream()
        {
            Stream responseStream = GetResponseStream();
            if (!ResponseContentType.ToLowerInvariant().Contains("xml"))
            {
                throw new NetworkException(NetworkErrorType.RESPONSE_ERROR, "Cofano.Net.Network:027 - Response was not in XML format.");
            }
            return new XmlResponseReader(responseStream);
        }

        public byte[] GetResponseBytes()
        {
            if (_Response != null)
                return _Response;

            try
            {
                using (Stream response = GetResponseStream())
                {
                    if (WebResponse.ContentLength > int.MaxValue)
                        throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:023 - Response cannot be bigger than " + int.MaxValue);
                    List<byte> responseBuffer = new List<byte>((int)WebResponse.ContentLength > 0 ? (int)WebResponse.ContentLength : 0);
                    byte[] buffer = new byte[2048];
                    long totalRead = 0;
                    int read = 0;

                    while ( (read = response.Read(buffer, 0, buffer.Length)) > 0 && totalRead < int.MaxValue)
                    {
                        responseBuffer.AddRange(buffer.Take(read));
                        totalRead += read;
                        if(totalRead > int.MaxValue)
                            throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:028 - Response cannot be bigger than " + int.MaxValue);
                    }
                    _Response = responseBuffer.ToArray();
                    responseBuffer.Clear();
                    responseBuffer = null;
                }

            }
            catch (Exception e)
            {
                if (e is NetworkException) throw;
                throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:022 - Error opening the stream of the response.", e);
            }

            if (_Response == null)
            {
                _Response = new byte[] { };
                throw new NetworkException("Cofano.Net.Network:024 - Response is empty.");
            }
            return _Response;
        }
        public string GetResponseString()
        {
            return new UTF8Encoding().GetString(GetResponseBytes());
        }
        private void MakeRequest()
        {
            bool multipart = _Files.Count > 0 && RequestType != NetworkRequestType.FILERAW;
            if (multipart)
                WebRequest.ContentType = "multipart/form-data; charset=utf-8; boundary=" + Boundary;

            if (RequestType == NetworkRequestType.FILERAW)
            {
                // If there was supplied a file, add that to the request stream.
                // If there was supplied a byte array, add that to the request stream.
                // If neither was supplied, throw an application exception.
                if (_Files.Count == 1)
                {
                    try
                    {
                        // Open the file, set the contentlength
                        FileInfo fileInfo = new FileInfo(_Files[0]);
                        WebRequest.ContentLength = fileInfo.Length;
                        try
                        {
                            // open a filestream with the file.
                            using(FileStream fileStream = fileInfo.OpenRead())
                            {
                                WriteStreamToRequest(fileStream);
                            }
                        } 
                        catch(Exception e)
                        {
                            if(e is NetworkException) throw e;
                            // FileInfo.OpenRead() threw exception.
                            /*  Possible exceptions:
                                UnauthorizedAccessException - path is read-only or is a directory.
                                DirectoryNotFoundException  - The specified path is invalid, such as being on an unmapped drive.
                                IOException                 - The file is already open.*/
                            throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:015 - Error opening the stream of the supplied file. (" + _Files[0] + ")", e);
                        }
                        
                    }
                    catch (Exception e)
                    {
                        if(e is NetworkException) throw;
                        // FileInfo threw exception.
                        /*  Possible exceptions:
                            ArgumentNullException       - FileName is null.
                            SecurityException           - The caller does not have the required permission.
                            ArgumentException           - The file name is empty, contains only white spaces, or contains invalid characters.
                            UnauthorizedAccessException - Access to fileName is denied.
                            PathTooLongException        - The specified path, file name, or both exceed the system-defined maximum length. For example, on Windows-based platforms, paths must be less than 248 characters, and file names must be less than 260 characters.
                            NotSupportedException       - fileName contains a colon (:) in the middle of the string.
                         */
                        throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:013 - Error opening the supplied file. (" + _Files[0] + ")", e);
                    }
                }
                else if (_PostBytes != null && _PostBytes.LongLength > 0)
                {
                    WriteBytesToRequest(_PostBytes);
                }
                else
                {
                    throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:014 - Must supply either a file or postdata when the RequestType is FILERAW.");
                }
            }
            if (RequestType == NetworkRequestType.POST)
            {
                if (_PostVars.Count != 0)
                {
                    StringBuilder postVars = new StringBuilder();
                    for (int i = 0; i < _PostVars.Count; i++)
                    {
                        if (i == 0)
                            postVars.Append(_PostVars[i].Key + "="+ _PostVars[i].Value);
                        else
                            postVars.Append("&" + _PostVars[i].Key + "=" + _PostVars[i].Value);
                    }
                    WriteBytesToRequest(new UTF8Encoding().GetBytes(postVars.ToString()));
                }
            }
            if(RequestType == NetworkRequestType.GET)
            {

            }
            /* #region hide
            throw new NotImplementedException();
            long length = 0;
            foreach(KeyValuePair<string,string> postPair in _PostVars)
            {
                // boundary -> --Boundary\r\n
                length += Boundary.Length + 4;
                // headers, ie -> Content-Disposition: form-data; name="submit-name"\r\n
                length += 43 + postPair.Key.Length;
                // new line, -> \r\n
                length += 2;
                // value -> Value\r\n
                length += postPair.Value.Length + 2;
            }
            foreach (string file in _Files)
            {
                // boundary -> --Boundary\r\n
                length += Boundary.Length + 4;
                // headers, ie -> Content-Disposition: form-data; name="files"; filename="file1.txt"\r\n
                length += 43;
            }
            #endregion
             * */
        }

        private void WriteBytesToRequest(byte[] bytes)
        {
            // set the content length
            WebRequest.ContentLength = bytes.LongLength;

            try
            {
                // open the request stream.
                using (Stream requestStream = WebRequest.GetRequestStream())
                {
                    try
                    {
                        requestStream.Write(bytes, 0, bytes.Length);
                    }
                    catch (Exception e)
                    {
                        throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:019 - Error transfering from the filestream to the requeststream.", e);
                    }
                }
            }
            catch (Exception e)
            {
                // WebRequest.GetRequestStream() threw exception.
                /*  Possible exceptions:
                    ProtocolViolationException  - The Method property is GET or HEAD. -or- KeepAlive is true, AllowWriteStreamBuffering is false, ContentLength is -1, SendChunked is false, and Method is POST or PUT.
                    InvalidOperationException   - The GetRequestStream method is called more than once. -or- TransferEncoding is set to a value and SendChunked is false.
                    NotSupportedException       - The request cache validator indicated that the response for this request can be served from the cache; however, requests that write data must not use the cache. This exception can occur if you are using a custom cache validator that is incorrectly implemented.
                    WebException                - Abort was previously called. -or- The time-out period for the request expired. -or- An error occurred while processing the request.
                    ObjectDisposedException     - In a .NET Compact Framework application, a request stream with zero content length was not obtained and closed correctly. For more information about handling zero content length requests, see Network Programming in the .NET Compact Framework.
                */
                throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:018 - Error opening the stream of the request.", e);
            }
        }

        private void WriteStreamToRequest(Stream stream)
        {
            try
            {
                // open the request stream.
                using (Stream requestStream = WebRequest.GetRequestStream())
                {
                    try
                    {
                        byte[] buffer = new byte[2048];
                        int read = 0;
                        while ((read = stream.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            requestStream.Write(buffer, 0, read);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:017 - Error transfering from the filestream to the requeststream.", e);
                    }
                }
            }
            catch (Exception e)
            {
                // WebRequest.GetRequestStream() threw exception.
                /*  Possible exceptions:
                    ProtocolViolationException  - The Method property is GET or HEAD. -or- KeepAlive is true, AllowWriteStreamBuffering is false, ContentLength is -1, SendChunked is false, and Method is POST or PUT.
                    InvalidOperationException   - The GetRequestStream method is called more than once. -or- TransferEncoding is set to a value and SendChunked is false.
                    NotSupportedException       - The request cache validator indicated that the response for this request can be served from the cache; however, requests that write data must not use the cache. This exception can occur if you are using a custom cache validator that is incorrectly implemented.
                    WebException                - Abort was previously called. -or- The time-out period for the request expired. -or- An error occurred while processing the request.
                    ObjectDisposedException     - In a .NET Compact Framework application, a request stream with zero content length was not obtained and closed correctly. For more information about handling zero content length requests, see Network Programming in the .NET Compact Framework.
                */
                throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:016 - Error opening the stream of the request.", e);
            }
        }

        public Network AddPostVar(string key, string value)
        {
            if (RequestType == NetworkRequestType.FILERAW)
                new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:007 - Cannot add post variables when the RequestType is FILERAW", new MethodAccessException("Not allowed."));
            _PostVars.Add(new KeyValuePair<string, string>(HttpUtility.UrlEncode(key), HttpUtility.UrlEncode(value)));
            return this;
        }

        public Network AddPostFile(string filePath)
        {
            if (RequestType == NetworkRequestType.FILERAW && (_Files.Count == 1 || _PostBytes != null))
                throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:008 - Cannot add more files when SetPostData or AddPostFile is already called, the RequestType is FILERAW.");

            try
            {
                using (Stream readStream = File.OpenRead(filePath)) 
                { 
                    if (readStream.CanRead)
                        this._Files.Add(filePath);
                }
            }
            catch (Exception e)
            {
                /*  Possible exceptions:
                    ArgumentNullException       - FileName is null.
                    SecurityException           - The caller does not have the required permission.
                    ArgumentException           - The file name is empty, contains only white spaces, or contains invalid characters.
                    UnauthorizedAccessException - Access to fileName is denied.
                    PathTooLongException        - The specified path, file name, or both exceed the system-defined maximum length. For example, on Windows-based platforms, paths must be less than 248 characters, and file names must be less than 260 characters.
                    NotSupportedException       - fileName contains a colon (:) in the middle of the string.
                    */
                throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:012 - Error opening the file provided in AddPostFile.", e);
            }
            return this;
        }
        
        public Network SetPostData(byte[] bytes)
        {
            if(RequestType != NetworkRequestType.FILERAW)
                throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:009 - This method is only valid with RequestType FILERAW.");
            if (_Files.Count >= 1)
                throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:010 - This method cannot be called after AddPostFile.");
            if (_PostBytes != null)
                throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:011 - This method can only be called once.");
            if (bytes.LongLength > int.MaxValue)
                throw new NetworkException(NetworkErrorType.PROGRAM_ERROR, "Cofano.Net.Network:020 - Max size in bytes = " + int.MaxValue.ToString());
            _PostBytes = bytes;
            return this;
        }
    }
}