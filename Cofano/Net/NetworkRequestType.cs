﻿namespace Cofano.Net
{
    public enum NetworkRequestType
    {
        FILE = 1,
        FILERAW = 2,
        POST = 4,
        GET  = 16
    }
}
