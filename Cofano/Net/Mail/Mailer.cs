﻿using System.Net.Mail;
using System.Net.Configuration;
using System.Configuration;
using System;
using System.Net;
using Cofano.Error;

namespace Cofano.Net.Mail
{
    /// <summary>
    /// Makes it easier to send mails quickly.
    /// AppSettings required:
    ///     System.Net/smtp.
    /// </summary>
    public class Mailer
    {
        private static string _MailFrom = null;
        private static string _SmtpUser = "no-reply@cofano.nl";
        private static string _SmtpPassword = "FDXmCX7R";
        private static string _SmtpServer = "mail.cofano.nl";
        private static int _SmtpPort = 25;

        /// <summary>
        /// The SmtpUser used to authenticate with the SmtpServer.
        /// Only used when no smtp credentials are given in the configuration.
        /// </summary>
        public static string SmtpUser
        {
            get { return Mailer._SmtpUser; }
            set { Mailer._SmtpUser = value; }
        }
        /// <summary>
        /// The SmtpPassword used to authenticate with the SmtpServer.
        /// Only used when no smtp credentials are given in the configuration.
        /// </summary>
        public static string SmtpPassword
        {
            get { return Mailer._SmtpPassword; }
            set { Mailer._SmtpPassword = value; }
        }
        /// <summary>
        /// The smtp server that sends the mail.
        /// Only used when no smtp credentials are given in the configuration.
        /// </summary>
        public static string SmtpServer
        {
            get { return Mailer._SmtpServer; }
            set { Mailer._SmtpServer = value; }
        }
        /// <summary>
        /// The port that smtp is running on.
        /// Only used when no smtp credentials are given in the configuration.
        /// </summary>
        public static int SmtpPort
        {
            get { return Mailer._SmtpPort; }
            set { Mailer._SmtpPort = value; }
        }

        /// <summary>
        /// The mail that is the sender of the mail.
        /// By default the smtp user.
        /// </summary>
        public static string MailFrom
        {
            get 
            {
                if (Mailer._MailFrom == null)
                    return Mailer.SmtpUser;
                return Mailer._MailFrom; 
            }
            set { Mailer._MailFrom = value; }
        }

        public static void SendMail(string recipient, string subject, string body, Action<MailMessage> changeMailMessage = null)
        {
            SendMail(MailFrom, recipient, subject, body, changeMailMessage);
        }
        public static void SendMail(string[] recipients, string subject, string body, Action<MailMessage> changeMailMessage = null)
        {
            SendMail(MailFrom, recipients, subject, body, changeMailMessage);
        }
        public static void SendMail(string sender, string recipient, string subject, string body, Action<MailMessage> changeMailMessage = null)
        {
            SendMail(sender, new string[] { recipient }, subject, body, changeMailMessage);
        }
        public static void SendMail(string sender, string[] recipients, string subject, string body, Action<MailMessage> changeMailMessage = null)
        {
            MailMessage email = new MailMessage();
            email.From = new MailAddress(sender);

            foreach (string recipient in recipients)
                email.To.Add(new MailAddress(recipient));

            email.Subject = subject;
            email.Body = body;
            email.IsBodyHtml = true;
            email.Priority = MailPriority.Normal;

            if (changeMailMessage != null)
                changeMailMessage(email);

            SmtpClient smtpClient = new SmtpClient();
            SmtpSection smtpSection = ConfigurationManager.GetSection("system.net/mailSettings/smtp") as SmtpSection;

            if (smtpSection == null || smtpSection.Network == null || smtpSection.Network.Host == null)
            {
                smtpClient.Host = Mailer.SmtpServer;
                smtpClient.Port = Mailer.SmtpPort;
                smtpClient.Credentials = new NetworkCredential(Mailer.SmtpUser, Mailer.SmtpPassword);
            }
            ServicePointManager.CheckCertificateRevocationList = false;
            ServicePointManager.ServerCertificateValidationCallback += (se, cert, chain, sslerror) =>
            {
                return true;
            };
            try
            {
                smtpClient.Send(email);
            }
            catch (Exception e)
            {
                throw new CofanoException("Mailer.cs:001, Couldn't send mail.", e);
            }
            finally
            {
                smtpClient.Dispose();
            }
        }
    }
}
