﻿using System;
using Cofano.Error;

namespace Cofano.Net
{
    public class NetworkException : CofanoException
    {
        
        private NetworkErrorType _Error;

        public NetworkErrorType Error
        {
            get { return _Error; }
            set { _Error = value; }
        }

        public NetworkException() : base()
        {
        }
        public NetworkException(string message)
            : base(message)
        {
        }
        public NetworkException(string message, Exception inner)
            : base(message, inner)
        {
        }

        public NetworkException(NetworkErrorType error)
            : base()
        {
            Error = error;
        }
        public NetworkException(NetworkErrorType error, string message)
            : base(message)
        {
            Error = error;
        }
        public NetworkException(NetworkErrorType error, string message, Exception inner)
            : base(message, inner)
        {
            Error = error;
        }

    }
}
