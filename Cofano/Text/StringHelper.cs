﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Cofano.Text
{
    public static class StringHelper
    {
        public static string RemoveExtraSpaces(this string text)
        {
            if (text.Contains("  "))
            {
                string left = "[-----";
                string right = "-----]";
                while (text.Contains(left) || text.Contains(right))
                {
                    left += "-";
                    right = "-" + right;
                }
                return text.Replace(" ", left + right).Replace(right + left, "").Replace(left + right, " ");
            }
            return text;
        }
    }
}
